import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { getAllUsers } from "./allusersReducer";
const reducers = combineReducers({ 
    users: userReducer,
    allusers:getAllUsers,
});

export default reducers;
