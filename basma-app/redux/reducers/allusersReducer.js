import { ActionTypes } from "../contants/action-types";

const initialState = {
  allusers: [],
};


export const getAllUsers = (
  state = initialState.allusers,
  { type, payload }
) => {
  switch (type) {
    case ActionTypes.GETALLUSERS:
      return {
        allusers: payload,
      };

    default:
      return state;
  }
};
