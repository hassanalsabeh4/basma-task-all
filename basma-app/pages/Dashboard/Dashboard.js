import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Icon } from "react-native-elements";
import { getAllUsers } from "../../redux/actions/alluserAction";
import { Picker } from "@react-native-picker/picker";

export default function CompanyPage({ navigation }) {
  const [value, setValue] = useState("");

  const allusersdata = useSelector((state) => state.allusers.allusers);
  console.log("ewwwwwwwwwwww", allusersdata);

  const dispatch = useDispatch();

  useEffect(() => {
    navigation.setOptions({
      title: "",

      headerRight: () => (
        <TouchableOpacity
          style={{ paddingRight: 10 }}
          onPress={() => navigation.navigate("Graph")}
        >
          <Text style={{ fontWeight: "bold", fontSize: 20 }}>Graph</Text>
        </TouchableOpacity>
      ),
    }),
      dispatch(getAllUsers());
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
        }}
      >
        <View style={styles.dropdown}>
          <Picker
            selectedValue={value}
            onValueChange={(value) => setValue(value)}
            style={styles.picker}
          >
            <Picker.Item
              label={"Filter"}
              value="0"
              style={styles.picker}
              enabled={false}
            />
            <Picker.Item label={"20"} value="20" style={styles.picker} />
            <Picker.Item label={"40"} value="40" style={styles.picker} />
            <Picker.Item label={"60"} value="60" style={styles.picker} />
          </Picker>
        </View>
        <View style={styles.dropdown}>
          <Picker
            selectedValue={value}
            onValueChange={(value) => setValue(value)}
            style={styles.picker}
          >
            <Picker.Item
              label={"Date"}
              value="0"
              style={styles.picker}
              enabled={false}
            />
            <Picker.Item label={"24 hours"} value="1" style={styles.picker} />
            <Picker.Item label={"last week"} value="7" style={styles.picker} />
            <Picker.Item
              label={"last month"}
              value="30"
              style={styles.picker}
            />
            <Picker.Item
              label={"last 3 months"}
              value="90"
              style={styles.picker}
            />
            <Picker.Item
              label={"last year"}
              value="365"
              style={styles.picker}
            />
          </Picker>
        </View>
      </View>
      <View style={styles.cardContainer}>
        {
          <View style={styles.cardHeaderContaner}>
            <Text style={styles.cardHeading}></Text>
          </View>
        }
        {allusersdata &&
          allusersdata.map((item) => {
            return (
              <View style={styles.cardBody}>
                <View style={styles.cardBodyTop}>
                  <View style={styles.cardLeftSide}>
                    <View>
                      <Text style={styles.cardName}>{item.name}</Text>

                      <Text style={styles.cardAddress}>{item.email}</Text>
                    </View>
                  </View>
                </View>
              </View>
            );
          })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rating: {
    flexDirection: "row",
    marginTop: 5,
  },
  tag: {
    color: "#B066A4",
  },
  cardContainer: {
    padding: 15,
    paddingBottom: 0,
  },

  picker: {
    marginVertical: -12,
    height: 5,
    fontSize: 16,
  },

  dropdown: {
    marginTop: "15%",
    backgroundColor: "#EE5A24",
    borderRadius: 10,
    height: 30,
    width: "30%",
  },
  margin: {
    height: 1,
    backgroundColor: "#F0F1F2",
    width: "100%",
    marginVertical: 10,
  },
  cardBodyBottom: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  cardBottomTitle: {
    fontSize: 14,
    marginTop: 5,
  },
  cardGroupIcon: {
    justifyContent: "center",
    alignItems: "center",
  },
  iconMore: {
    position: "absolute",
    bottom: 0,
    right: 0,
  },
  iconLike: {
    position: "absolute",
    top: 0,
    right: 0,
  },
  cardBody: {
    padding: 15,
    backgroundColor: "#fff",
    marginTop: 15,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  cardBodyTop: {
    flexDirection: "row",
  },
  cardLeftSide: {
    paddingHorizontal: 10,
    flex: 1,
  },
  cardName: {
    color: "#222",
    fontSize: 18,
    fontWeight: "bold",
  },
  cardTime: {
    color: "#222",
    fontSize: 16,
    fontWeight: "500",
    marginTop: 5,
  },
  cardAddress: {
    color: "gray",
    fontSize: 15,
    fontWeight: "500",
    marginTop: 5,
  },
  cardAvatar: {
    height: 100,
    width: 100,
    backgroundColor: "gray",
    borderRadius: 10,
  },
  cardHeaderContaner: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  cardHeading: {
    fontSize: 24,
    fontWeight: "bold",
  },
  cardMore: {
    fontWeight: "bold",
    color: "#7B6C95",
  },
  faceGroup: {
    justifyContent: "center",
    alignItems: "center",
  },
  faceContainer: {
    backgroundColor: "#fff",
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 20,
    marginHorizontal: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    marginTop: 20,
  },
  faceText: {
    fontSize: 16,
    marginTop: 6,
  },

  container: {
    flex: 1,
  },
  headerContainer: {
    padding: 20,
    paddingHorizontal: 30,
    marginTop: 52,
  },
  heading: {
    fontSize: 32,
    fontWeight: "bold",
    color: "#fff",
  },
  desc: {
    fontSize: 20,
    fontWeight: "400",
    color: "#fff",
    marginTop: 5,
  },
  buttonBooks: {
    flexDirection: "row",
    marginTop: 20,
  },
  btnGradient: {
    padding: 10,
    borderRadius: 40,
  },
  btnBookText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#fff",
  },
});
