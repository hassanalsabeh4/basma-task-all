import React from "react";
import "./Friends.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import phone from "../../img/thumb-3.png";
export default function Friends() {
  return (
    <div className="friends">
      <div className="friendstext">
        <h1>Share your photos with friends easily</h1><FontAwesomeIcon icon="fa-solid fa-layer-group" />
        <div className="friendslist">
          <ul>
            <li>
            <FontAwesomeIcon icon="fa-solid fa-layer-group" />
              {" "}
              Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            </li>
            <li>
              {" "}
              Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            </li>
            <li>
              {" "}
              Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            </li>
            <li>
              {" "}
              Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            </li>
            <li>
              {" "}
              Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            </li>
          </ul>
        </div>

        <button className="btn-grad ">Learn More</button>
      </div>
      <img className="phone3" src={phone} />
    </div>
  );
}
