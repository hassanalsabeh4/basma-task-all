import React from "react";
import "./CodeChallenge.css";
import pointer from "../../img/pointer-top.svg";
import chrome from "../../img/chromecast.svg";
import settings from "../../img/500px.svg";
import arrow from "../../img/arrow.svg";

export default function CodeChallenge() {
  return (
    <div className="codechallenge">
      <div className="codechallengetitle">
        <h1>Code Challenge</h1>
        <br></br>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum.  <ion-icon name="layers-outline"></ion-icon>
        </p>
      </div>
 
      <div className="grid-container1">
        <div className="icondesc1">
          <img
            src={pointer}
            width="70"
            height="70"
            style={{ marginLeft: "22%" }}
          />
          <h2>Fully Functional</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>
        <img className="arrow"
            src={arrow}
            width="70"
            height="70"
            style={{ marginTop: "122%" }}
          />
        <div className="icondesc1">
          <img
            src={chrome}
            width="70"
            height="70"
            style={{ marginLeft: "22%" }}
          />
          <h2>Fully Functional</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>{" "}
         <img
         className="arrow"
            src={arrow}
            width="70"
            height="70"
            style={{ marginTop: "122%" }}
          />
        <div className="icondesc1">
          <img
            src={settings}
            width="70"
            height="70"
            style={{ marginLeft: "22%" }}
          />
          <h2>Location Tracking</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>{" "}
      </div>
    </div>
  );
}
