import React from "react";
import "./BasmaCode.css";
import bike from "./bike.png";
import motorbike from "./motorbike.png";

export default function BasmaCode() {
  return (
    <div className="basma">
      <div className="basmatitle">
        <h1>Basma Code Challenge</h1>
        <hr></hr>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum.
        </p>
      </div>
      <div className="grid-container2">
        <div className="icondesc2">
          <img
            src={bike}
            width="70"
            height="70"
            style={{ marginLeft: "35%" }}
          />
          <h2>BASIC</h2>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            {" "}
            <h4 style={{ marginTop: "17%", color: "#7200e5" }}>$</h4>
            <h1>49</h1>
          </div>
          <p> Lorem ipsum odor amet</p>
          <hr></hr>
          <p> Lorem ipsum odor amet</p>
          <hr></hr>
          <p> Lorem ipsum</p>
          <hr></hr>
          <p> Lorem ipsum</p>
          <hr></hr>
          <button className="btn-grad3">Sign Up</button>
        </div>
        <div className="icondesc2">
          <img
            src={motorbike}
            width="70"
            height="70"
            style={{ marginLeft: "35%" }}
          />
          <h2>Pro</h2>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            {" "}
            <h4 style={{ marginTop: "17%", color: "#7200e5" }}>$</h4>
            <h1>129</h1>
          </div>

          <p> Lorem ipsum odor amet</p>
          <hr></hr>
          <p> Lorem ipsum odor amet</p>
          <hr></hr>
          <p> Lorem ipsum</p>
          <hr></hr>
          <p> Lorem ipsum</p>
          <hr></hr>
          <button className="btn-grad3">Sign Up</button>
        </div>{" "}
       
      </div> <p className="contact">Not sure wht you choose?<a href="#">Contact Us</a></p>
    </div>
  );
}
