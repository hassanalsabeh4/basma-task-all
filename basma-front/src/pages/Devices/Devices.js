import React from "react";
import "./Devices.css";
import google from "./google-play.png";
import appstore from "./app-store.png"


export default function Devices() {
  return (
    <div className="devices">
      <div className="devicestitle">
        <h1>Basma is available for all devices</h1>
        <br></br>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum.Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis elementum; dignissim elementum.
        </p>
      </div>

      <div className="grid-container4">
        <div className="icondesc4">
          <img
            src={google}
            width="70"
            height="70"
          />
         
        </div>
        <div className="icondesc4">
        <img
            src={appstore}
            width="70"
            height="70"
          />
          
        </div>{" "}
      </div>
      <p>
          {" "}
         * Lorem ipsum odor amet, consectetuer adipiscing elit.
        </p>
    </div>
  );
}
