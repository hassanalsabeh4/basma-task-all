import React from "react";
import "./Subscribe.css";

export default function Subscribe() {
  return (
    <div className="subscribe">
      <div className="subscribetitle">
        <h1>Subscribe to get updates</h1>
        <br></br>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum.
        </p>
      </div>

      <div className="grid-container5">
        <input type="text" placeholder="Enter your email"></input>
        <button className="btn-grad5 ">Subscribe</button>
      </div>
    </div>
  );
}
