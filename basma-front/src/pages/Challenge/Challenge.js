import React from "react";
import "./Challenge.css";
import pointer from "../../img/pointer-top.svg";
import chrome from "../../img/chromecast.svg";
import settings from "../../img/500px.svg";
import offer from "../../img/offer.svg";
import lock from "../../img/lock-alt.svg";
import laravel from "../../img/laravel.svg"
export default function Challenge() {
  return (
    <div className="challenge">
      <div className="challengetitle">
        <h1>Code Challenge</h1>
        <hr></hr>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum.
        </p>
      </div>
      <div className="grid-container">
        <div className="icondesc">
          <img src={pointer} width="70" height="70" style={{marginLeft:"22%"}} />
          <h2>Fully Functional</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>
        <div className="icondesc">
          <img src={chrome} width="70" height="70" style={{marginLeft:"22%"}}/>
          <h2>Fully Functional</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>{" "}
        <div className="icondesc">
          <img src={settings} width="70" height="70" style={{marginLeft:"22%"}}/>
          <h2>Location Tracking</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>{" "}
        <div className="icondesc">
          <img src={offer} width="70" height="70" style={{marginLeft:"22%"}}/>
          <h2>Powerfull Settings</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>{" "}
        <div className="icondesc">
          <img src={lock} width="70" height="70" style={{marginLeft:"22%"}}/>
          <h2>Multiple Language</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>{" "}
        <div className="icondesc">
          <img src={laravel} width="70" height="70" style={{marginLeft:"22%"}}/>
          <h2>Fully Functional</h2>{" "}
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
        </div>
      </div>
    </div>
  );
}
