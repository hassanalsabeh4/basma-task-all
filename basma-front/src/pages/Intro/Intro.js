import React from "react";
import "./Intro.css";
import phone from "../../img/thumb-1.png";
export default function Intro() {
  return (
    <div className="intro">
      <div className="introtext">
        <h1>Creative way to Showcase your App</h1>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum. Mollis tincidunt mattis
          hendrerit dolor eros enim, nisi ligula ornare.eros enim, nisi ligula ornare.
        </p>
        
        <button className="btn-grad ">
            Get Started
        </button>
      </div>
      <img className="phone" src={phone} />
    </div>
  );
}
