import React from "react";
import "./Navbar.css";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <div>
      <nav>
        <div class="hamburger">
          <div class="line1"></div>
          <div class="line2"></div>
          <div class="line3"></div>
        </div>
        <ul class="nav-links">
          <li>
            <a>
              {" "}
              <select id="dropdown">
                <option value="N/A">Home</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
              </select>
            </a>
          </li>
          <li>
            <a href="#">Features</a>
          </li>
          <li>
            <a>
              {" "}
              <select id="dropdown">
                <option value="N/A">Pages</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
              </select>
            </a>
          </li>
          <li>
            <a href="#">Screenshots</a>
          </li>
          <li>
            <a href="#">Pricing</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
      </nav>
      <script src="nav.js"></script>
    </div>
  );
}
