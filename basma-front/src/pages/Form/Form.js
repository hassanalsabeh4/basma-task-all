import React from "react";
import "./Form.css";

export default function Form() {
  return (
    <div className="form">
      <div className="formtitle">
        <h1>Stay Tunned</h1>
        <hr></hr>
        <p>
          {" "}
          Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa
          egestas mollis varius; dignissim elementum.
        </p>
      </div>
      <div className="grid-container6">
        <div className="icondesc6">
      
          <p>
            {" "}
            Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in
            massa.
          </p>
          <ul>
            <li>
           
              {" "}
              Lorem ipsum odor amet, consectetuer 
            </li>
            <li>
              {" "}
              +1230 456 789 - 012 345 6789
            </li>
            <li>
           
           {" "}
           Lorem ipsum odor amet, consectetuer 
         </li>
          </ul>
        </div>
        <div className="icondesc6">
          <div>
            <input type="text" placeholder="Name"></input>
            <input type="text" placeholder="Email"></input>
            <input type="text" placeholder="Subject"></input>
            <textarea
              rows="4"
              cols="50"
              name="comment"
              form="usrform"
              placeholder="Message"
            ></textarea>
            <button className="btn-grad7 ">Send Message</button>
          </div>
        </div>{" "}
      </div>
    </div>
  );
}
