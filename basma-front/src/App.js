import React from "react";
import Shape from "react-clip-path";
import Navbar from "./pages/Navbar/Navbar";
import Intro from "./pages/Intro/Intro";
import Challenge from "./pages/Challenge/Challenge";
import Powerfull from "./pages/Powerfull/Powerfull";
import Friends from "./pages/Friends/Friends";
import CodeChallenge from "./pages/CodeChallenge/CodeChallenge";
import BasmaCode from "./pages/BasmaCode/BasmaCode";
import Devices from "./pages/Devices/Devices";
import Subscribe from "./pages/Subscribe/Subscribe";
import Form from "./pages/Form/Form";
import "./App.css"
function App() {
  return (
    <div style={{ margin: -8 , fontFamily: "Arial"}}>
      <div style={{position:"relative"}} className="polygon">
        <Shape
        
          width="100%"
          height="200vh"
          showLabel={true}
          backgroundColor="#5812ed"
          formula="polygon(47% 0, 100% 0, 100% 33%, 72% 52%)"
        />
      </div>
      <div >
      <Navbar />
      <Intro />
      <Challenge />
      <Powerfull />
      <Friends />
      <CodeChallenge />
      <BasmaCode />
      <Devices />
      <Subscribe />
      <Form />
      </div>
    
    </div>
  );
}

export default App;
