import { ActionTypes } from "../contants/action-types";

const initialState = {
  users: {},
};

export const usersReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.LOGINUSER:
      debugger;
      return {
        users: payload,
      };
    case ActionTypes.ADDUSER:
      return {
        users: payload,
      };
    default:
      return state;
  }
};
