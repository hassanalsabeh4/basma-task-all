import { combineReducers } from "redux";

import { usersReducer } from "./usersReducer";

const reducers = combineReducers({
  data: usersReducer,
});

export default reducers;
