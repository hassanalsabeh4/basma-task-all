import urlAxios from "../../apis/axiosApi";
import { ActionTypes } from "../contants/action-types";
import Swal from "sweetalert2";

export const loginUsers = (object, navigate) => {
  return async function (dispatch) {
    try {
      const response = await urlAxios.post("/login", object, {
        headers: {
          "Content-type": "application/json",
          Accept: "application/json",
        },
      });
      const data = response.data;
      if (data.success) {
        localStorage.setItem("token", data.access_token);
        localStorage.setItem("id", data.data.id);
        debugger;
        dispatch({ type: ActionTypes.LOGINUSER, payload: data.data });
        navigate("/Dashboard");
        Swal.fire("Good job!", "Logged in successfully", "success");
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
      }
      console.log(err.message);

      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Wrong Username or Password login!   Try Again",
      });
    }
  };
};

export const addUsers = (object, navigate) => {
  return async function (dispatch) {
    try {
      const response = await urlAxios.post(`/register`, object, {
        headers: {
          "Content-type": "application/json",
          Accept: "application/json",
        },
      });
      const data = response.data;
      if (data.success) {
        dispatch({ type: ActionTypes.ADDUSER, payload: data.data });
        navigate("/");
        Swal.fire("Good job!", "Registered successfully", "success");
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Wrong details! Try Again",
        });
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response.message);
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Wrong details! Try Again",
        });
      }
      console.log(err.message);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Some error occured",
      });
    }
  };
};
